# 🚀 zkygr.zproxmox

**!!! WIP !!!**

This collection bundles a set of roles to manage a docker environment inside of proxmox virtual machines.

## Dependencies

- python version: `3.9`
- python packages:
  - `jsonpath-ng`
  - `mock`
  - `pytest`

## Content 🔖

- ✅ [zvm](https://gitlab.com/ninjadogs/ansible-collection-zproxmox/-/tree/main/zproxmox/roles/zvm)
  - create, update, delete, start and stop virtual machines
- ✅ [zprovision](https://gitlab.com/ninjadogs/ansible-collection-zproxmox/-/tree/main/zproxmox/roles/zprovision)
  - install docker (optional)
  - install common packages
  - mount nfs shares (truenas)
- 🚫 ztemplate
  - create, update and delete templates
- ✅ [zcompose](https://gitlab.com/ninjadogs/ansible-collection-zproxmox/-/tree/main/zproxmox/roles/zcompose)
  - start a docker stack
  - remove a docker stack
  - list all containers

\*🚫 - Not implemented yet
