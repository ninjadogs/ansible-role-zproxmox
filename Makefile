.ONESHELL:
SHELL := /bin/bash
.DEFAULT_GOAL := help

# Path variables
GIT_ROOT := $(shell git rev-parse --show-toplevel)

# Text colors
yellow = $(shell tput setaf 3)
magenta = $(shell tput setaf 5)
reset = $(shell tput sgr0)

# -----------------------------------------------------------------------------
# TARGETS
# -----------------------------------------------------------------------------

.PHONY: help
help: ## Displays all available targets with their descriptions
	@awk 'BEGIN { FS = ":.*##" } \
        /^[a-zA-Z_-]+:.*?##/ { \
          printf "$(yellow)%-25s$(reset) $(magenta)%s$(reset)\n", $$1, $$2 \
        }' $(MAKEFILE_LIST) | sort

.PHONY: setup-fedora
setup-fedora: ## Setup the virtual environment on a fedora system
	sudo dnf install -y pipenv
	pipenv install --ignore-pipfile --dev
	make hooks-update

.PHONY: hooks-update
hooks-update: ## Installs and updates git hooks
	pipenv run pre-commit install
	pipenv run pre-commit install --hook-type post-commit
	pipenv run pre-commit autoupdate

.PHONY: linters-run
linters-run: ## Runs ansible-lint and yamllint
	pipenv run ansible-lint $(GIT_ROOT)
	pipenv run yamllint $(GIT_ROOT)
