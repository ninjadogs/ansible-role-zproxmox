from abc import ABC, abstractmethod
import json


try:
    from ansible_collections.zkygr.zproxmox.plugins.module_utils.ansible_module_helper import (
        AnsibleModuleHelper,
    )
    from ansible_collections.zkygr.zproxmox.plugins.module_utils.truenas_api_client import (
        TruenasApiClient,
    )
except:
    from module_utils.truenas_api_client import TruenasApiClient
    from module_utils.ansible_module_helper import AnsibleModuleHelper


class TruenasModule(ABC):
    data = {}

    @abstractmethod
    def set_data(self):
        pass

    def __init__(
        self, module_helper: AnsibleModuleHelper, api_client: TruenasApiClient, params
    ):
        self.module_helper = module_helper
        self.api_client = api_client
        self.params = params
        self.set_data()

    def add_conditional_data(self, guard_key, state, data_key):
        self.data[guard_key] = self.get_param(guard_key)
        guard_value = self.get_param(guard_key)
        if guard_value == state:
            self.data[data_key] = self.get_param(data_key)

    def add_param_to_data(self, key, value=None):
        if value is None:
            if self.params[key] is not None:
                self.data[key] = self.params[key]
        else:
            self.data[key] = value

    def get_data_json(self):
        data_json = json.dumps(self.data)
        return data_json

    # TODO: rename param accept_none
    def get_param(self, key, fail_if_value_is_none=True):
        value = self.params[key]
        if value is None and fail_if_value_is_none:
            self.module_helper.fail_json(f"There was no param with the key '{key}'")

        return value

    def remove_from_data(self, *keys):
        for key in keys:
            del self.data[key]
