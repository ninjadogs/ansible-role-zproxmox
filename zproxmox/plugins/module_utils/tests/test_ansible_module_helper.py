from module_utils.ansible_module_helper import AnsibleModuleHelper
from unittest.mock import patch
import pytest


class TestAnsibleModuleHelper:
    @pytest.fixture
    @patch("ansible.module_utils.basic.AnsibleModule")
    def helper(self, mock):
        return AnsibleModuleHelper(mock)

    def test_has_changed_empty_request(self, helper):
        request = {}
        response = {"key1": "value1", "key2": "value2"}
        result = helper.has_changed(request, response)
        assert result is True

    def test_has_changed_same_values(self, helper):
        request = {"key1": "value1", "key2": "value2"}
        response = {"key1": "value1", "key2": "value2"}
        result = helper.has_changed(request, response)
        assert result is False

    def test_has_changed_different_values(self, helper):
        request = {"key1": "value1", "key2": "value2"}
        response = {"key1": "value1", "key2": "new_value"}
        result = helper.has_changed(request, response)
        assert result is True

    def test_has_changed_extra_key_in_response(self, helper):
        request = {"key1": "value1", "key2": "value2"}
        response = {"key1": "value1", "key2": "value2", "key3": "value3"}
        result = helper.has_changed(request, response)
        assert result is False
