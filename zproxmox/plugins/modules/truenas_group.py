#!/usr/bin/python

# Copyright: (c) 2018, Terry Jones <terry.jones@example.org>
# GNU General Public License v3.0+ (see COPYING or https://www.gnu.org/licenses/gpl-3.0.txt)
try:
    from ansible_collections.zkygr.zproxmox.plugins.module_utils.ansible_module_helper import (
        AnsibleModuleHelper,
    )
    from ansible_collections.zkygr.zproxmox.plugins.module_utils.truenas_api_client import (
        TruenasApiClient,
    )
    from ansible_collections.zkygr.zproxmox.plugins.module_utils.truenas_module import (
        TruenasModule,
    )
except:
    from module_utils.truenas_api_client import TruenasApiClient
    from module_utils.truenas_module import TruenasModule
    from module_utils.ansible_module_helper import AnsibleModuleHelper

__metaclass__ = type

DOCUMENTATION = r"""
---
module: truenas_group
short_description: Create, update  or delete groups in truenas.
version_added: "1.0.0"
description: This module can create or delete truenas groups.

options:
    url: 
        description: The url of the truenas server.
        required: true
        type: str
    api_token: 
        description: The api token for the truenas server.
        required: true
        type: str
    verify: 
        description: Whether to verify the ssl certificate or not 
        required: false
        type: bool
    state: 
        description: Whether to create (present) or remove (absent) the group.
        required: true
        type: choices ['present', 'absent']
    gid: 
        description: If `gid` is not provided it is automatically filled with the next one available.        
        required: false
        type: str
    name: 
        description: The name of the group.
        required: true
        type: str
    smb: 
        description: Set to allow group to be used for Samba permissions and authentication.
        required: false
        type: bool
    allow_duplicate_gid: 
        description: Allows distinct group names to share the same gid.
        required: false
        type: false
    users: 
        description: a list of user ids (`id` attribute from `user.query`).        
        required: false
        type: list
    sudo_commands: 
        description: List of allowed sudo commands
        required: false
        type: list
    sudo_commands_nopasswd: 
        description: List of allowed sudo commands without password
        required: false
        type: list

author:
    - Kay Gerlitzki (@zkygr)
"""

EXAMPLES = r"""
- name: Add group foo
  zkygr.zproxmox.truenas_group:
    url: https://truenas.example.com
    api_token: ****
    verify: true
    name: foo
    gid: 4000
"""


def get_module_args():
    return dict(
        url=dict(type="str", required=True),
        api_token=dict(type="str", required=True),
        verify=dict(type="bool", default=False, required=False),
        state=dict(default="present", choices=["present", "absent"]),
        gid=dict(type="str", required=False),
        name=dict(type="str", required=False),
        smb=dict(type="bool", default=False, required=False),
        allow_duplicate_gid=dict(type="bool", default=False, required=False),
        users=dict(type="list", elements=str, required=False),
        sudo_commands=dict(type="list", elements=str, required=False),
        sudo_commands_nopasswd=dict(type="list", elements=str, required=False),
    )


# -----------------------------------------------------------------------------


class TruenasGroupModule(TruenasModule):
    def __init__(self, module_helper, api_client, params):
        super().__init__(module_helper, api_client, params)

    # -------------------------------------------------------------------------

    def run_module(self):
        self.module_helper.handle_check_mode(changed=False, response="")
        self.module_helper.set_request(self.data)

        state = self.get_param("state")
        group = self.api_client.find_item_by_property(
            "group", "name", self.get_param("name")
        )
        group_id = ""
        group_exists = False

        if group is not None and group["id"] is not None:
            group_exists = True
            group_id = int(group["id"])
            api_endpoint = f"group/id/{group_id}"
        else:
            api_endpoint = "group"

        if state == "present" and group_exists:
            self.api_client.put(api_endpoint, self.data)
            response = self.get_group(group_id)
            self.module_helper.set_response(response)
            has_changed = self.module_helper.has_changed(group, response)
            self.module_helper.set_changed(has_changed)

        elif state == "absent" and group_exists:
            self.api_client.delete(api_endpoint)
            self.module_helper.set_response(
                f"The group with the group id {group_id} was deleted"
            )
            self.module_helper.set_changed(True)

        elif state == "present" and not group_exists:
            group_id = self.api_client.post("group", self.data)
            self.module_helper.check_for_error(group_id)

            new_group = self.get_group(group_id)
            self.module_helper.set_response(new_group)
            self.module_helper.set_changed(True)

        elif state == "absent" and not group_exists:
            self.module_helper.set_response("Group was already absent")
            self.module_helper.set_changed(False)

        self.module_helper.exit_json()

    # -------------------------------------------------------------------------

    def get_group(self, group_id):
        return self.api_client.find_item_by_property("group", "id", group_id)

    def set_data(self):
        self.add_param_to_data("gid")
        self.add_param_to_data("name")
        self.add_param_to_data("smb")
        self.add_param_to_data("sudo_commands")
        self.add_param_to_data("sudo_commands_nopasswd")
        self.add_param_to_data("allow_duplicate_gid")
        self.add_param_to_data("users")


# -----------------------------------------------------------------------------


def main():
    module_args = get_module_args()
    module = AnsibleModuleHelper.get_module(module_args)
    module_helper = AnsibleModuleHelper(module)
    module_helper.set_no_log_values("api_token")

    base_url = module_helper.get_param("url")
    verify = module_helper.get_param("verify")
    api_token = module_helper.get_param("api_token")
    api_client = TruenasApiClient(base_url, api_token, bool(verify))

    params = module_helper.params
    truenas_module = TruenasGroupModule(module_helper, api_client, params)
    truenas_module.run_module()


if __name__ == "__main__":
    main()
