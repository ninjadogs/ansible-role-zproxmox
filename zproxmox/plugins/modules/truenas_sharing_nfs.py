#!/usr/bin/python

# Copyright: (c) 2018, Terry Jones <terry.jones@example.org>
# GNU General Public License v3.0+ (see COPYING or https://www.gnu.org/licenses/gpl-3.0.txt)
try:
    from ansible_collections.zkygr.zproxmox.plugins.module_utils.ansible_module_helper import (
        AnsibleModuleHelper,
    )
    from ansible_collections.zkygr.zproxmox.plugins.module_utils.truenas_api_client import (
        TruenasApiClient,
    )
    from ansible_collections.zkygr.zproxmox.plugins.module_utils.truenas_module import (
        TruenasModule,
    )
except:
    from module_utils.truenas_api_client import TruenasApiClient
    from module_utils.truenas_module import TruenasModule
    from module_utils.ansible_module_helper import AnsibleModuleHelper

__metaclass__ = type

DOCUMENTATION = r"""
---
module: truenas_nfs_share
short_description: Manage truenas nfs shares
version_added: "1.0.0"
description: This module can create, update or delete nfs shares

options:
    url: 
        description: The url of the truenas server.
        required: true
        type: str
    api_token: 
        description: The api token for the truenas server.
        required: true
        type: str
    verify: 
        description: Whether to verify the ssl certificate or not 
        required: false
        type: bool
    path:
        description: The path to share from the the truenas server.
        required: true
        type: str
    state: 
        description: Whether to create or update(present), or remove (absent) the nfs share.
        required: true
        type: choices ['present', 'absent']
    comment: 
        description: Add a comment in truenas to the nfs share.
        required: false
        type: str
    hosts: 
        description: A list of hosts which can access the nfs share.
        required: false
        type: list(str)
        default: []
    networks:
        description: A list of networks that can access the nfs share.
        required: false
        type: list(str)
        default: []
    mapall_group:
        description: The specified permissions of that group are used by all clients.
        required: false
        type: str
        default: ""
    mapall_user:
        description: The specified permissions of that user are used by all clients.
        required: false
        type: str
        default: ""
    maproot_group:
        description: When a group is selected, the root user is also limited to the permissions of that group.
        required: false
        type: str
        default: ""
    maproot_user:
        description: When a user is selected, the root user is limited to the permissions of that user.
        required: false
        type: str
        default: ""


# Specify this value according to your collection
# in format of namespace.collection.doc_fragment_name
extends_documentation_fragment:
    - my_namespace.my_collection.my_doc_fragment_name

author:
    - Kay Gerlitzki (@zkygr)
"""

EXAMPLES = r"""
# Pass in a message
- name: Create or update nfs share for photos
  truenas_nfs_share:
      url: https://truenas.my.domain
      api_token: ****
      path: /mnt/pool1/photos
      comment: Share my photos directory
      verify: true
      hosts:
          - 192.168.1.10
      state: present

- name: Delete my photos nfs share
  truenas_nfs_share:
      url: https://truenas.my.domain
      api_token: ****
      path: /mnt/pool1/photos
      state: absent

"""


def get_module_args():
    return dict(
        url=dict(type="str", required=True),
        api_token=dict(type="str", required=True),
        path=dict(type="str", required=True),
        state=dict(default="present", choices=["present", "absent"]),
        comment=dict(type="str", default="", required=False),
        verify=dict(type="bool", default=False, required=False),
        hosts=dict(type="list", elements=str, default=[], required=False),
        networks=dict(type="list", elements=str, default=[], required=False),
        mapall_user=dict(type="str", default="", required=False),
        mapall_group=dict(type="str", default="", required=False),
        maproot_user=dict(type="str", default="", required=False),
        maproot_group=dict(type="str", default="", required=False),
    )


# -----------------------------------------------------------------------------


class TruenasSharingNfsModule(TruenasModule):
    def __init__(self, module_helper, api_client, params):
        super().__init__(module_helper, api_client, params)

    def run_module(self):
        self.module_helper.set_request(self.data)
        self.module_helper.handle_check_mode(False, "")

        state = self.module_helper.get_param("state")
        path = self.module_helper.get_param("path")
        nfs_share = self.api_client.find_item_by_property("sharing/nfs", "path", path)

        if nfs_share is not None and nfs_share["id"] is not None:
            nfs_id = nfs_share["id"]
            api_endpoint = f"sharing/nfs/id/{nfs_id}"

            if state == "present":
                self.module_helper.set_request(nfs_share)
                response = self.api_client.put(api_endpoint, self.data)
                self.module_helper.set_response(response)

                has_changed = self.module_helper.has_changed(nfs_share, response)
                self.module_helper.set_changed(has_changed)

            elif state == "absent":
                response = self.api_client.delete(api_endpoint)
                self.module_helper.set_response(response)
                self.module_helper.set_changed(True)

        else:
            if state == "present":
                response = self.api_client.post("sharing/nfs", self.data)
                self.module_helper.set_response(response)
                self.module_helper.set_changed(True)

            elif state == "absent":
                self.module_helper.set_response("Nfs share is already absent")

        self.module_helper.exit_json()

    def set_data(self):
        self.data = {
            "path": self.module_helper.get_param("path"),
            "networks": self.module_helper.get_param("networks"),
            "hosts": self.module_helper.get_param("hosts"),
            "comment": self.module_helper.get_param("comment"),
            "mapall_user": self.module_helper.get_param("mapall_user"),
            "mapall_group": self.module_helper.get_param("mapall_group"),
            "maproot_user": self.module_helper.get_param("maproot_user"),
            "maproot_group": self.module_helper.get_param("maproot_group"),
        }


# -----------------------------------------------------------------------------


def main():
    module_args = get_module_args()
    module = AnsibleModuleHelper.get_module(module_args)
    module_helper = AnsibleModuleHelper(module)

    base_url = module_helper.get_param("url")
    verify = module_helper.get_param("verify")
    api_token = module_helper.get_param("api_token")
    api_client = TruenasApiClient(base_url, api_token, bool(verify))

    params = module_helper.params
    truenas_module = TruenasSharingNfsModule(module_helper, api_client, params)
    truenas_module.run_module()


if __name__ == "__main__":
    main()
