#!/usr/bin/python

# Copyright: (c) 2018, Terry Jones <terry.jones@example.org>
# GNU General Public License v3.0+ (see COPYING or https://www.gnu.org/licenses/gpl-3.0.txt)
try:
    from ansible_collections.zkygr.zproxmox.plugins.module_utils.ansible_module_helper import (
        AnsibleModuleHelper,
    )
    from ansible_collections.zkygr.zproxmox.plugins.module_utils.truenas_api_client import (
        TruenasApiClient,
    )
    from ansible_collections.zkygr.zproxmox.plugins.module_utils.truenas_module import (
        TruenasModule,
    )
except:
    from module_utils.truenas_api_client import TruenasApiClient
    from module_utils.truenas_module import TruenasModule
    from module_utils.ansible_module_helper import AnsibleModuleHelper

__metaclass__ = type

DOCUMENTATION = r"""
---
module: truenas_cronjob
short_description: Create, update or delete truenas cronjobs.
version_added: "1.0.0"
description: This module can create, update or delete truenas cronjobs.

options:
    url: 
        description: The url of the truenas server.
        required: true
        type: str
    api_token: 
        description: The api token for the truenas server.
        required: true
        type: str
    verify: 
        description: Whether to verify the ssl certificate or not 
        required: false
        type: bool
    state: 
        description: Whether to create, update (present) or remove (absent) the cronjob.
        required: true
        type: choices ['present', 'absent']
    enabled: 
        description: Whether the cronjob is enabled or disabled.
        required: false
        type: bool
    description: 
        description: The description of the cronjob.
        required: false
        type: str
    stderr: 
        description: When unset, any error output is mailed to the user account cron used to run the command.
        required: false
        default: false
        type: bool
    stdout: 
        description: When unset, any standard output is mailed to the user account cron used to run the command.
        required: false
        default: true
        type: bool
    minute: 
        description: The minute in which the cronjob will run.
        required: true
        default: "*"
        type: str
    hour: 
        description: The hour in which the cronjob will run.
        required: true
        default: "*"
        type: str
    dom: 
        description: The day of month in which the cronjob will run.
        required: true
        default: "*"
        type: str
    month: 
        description: The month in which the cronjob will run.
        required: true
        default: "*"
        type: str
    dow: 
        description: The day of the week to run the cronjob.
        required: true
        default: "*"
        type: str
    command:
        description: The command the cronjob will run.
        required: true
        type: str
    user:
        description: The user (username) who will run the cronjob.
        required: true
        type: str

author:
    - Kay Gerlitzki (@zkygr)
"""

EXAMPLES = r"""
- name: Add group foo
  zkygr.zproxmox.truenas_group:
    url: https://truenas.example.com
    api_token: ****
    verify: true
    name: foo
    gid: 4000
"""


def get_module_args():
    return dict(
        url=dict(type="str", required=True),
        api_token=dict(type="str", required=True),
        verify=dict(type="bool", default=False, required=False),
        state=dict(default="present", choices=["present", "absent"]),
        enabled=dict(type="bool", default=False, required=False),
        stdout=dict(type="bool", default=False, required=False),
        stderr=dict(type="bool", default=True, required=False),
        minute=dict(type="str", default="*", required=False),
        hour=dict(type="str", default="*", required=False),
        dom=dict(type="str", default="*", required=False),
        month=dict(type="str", default="*", required=False),
        dow=dict(type="str", default="*", required=False),
        command=dict(type="str", required=True),
        user=dict(type="str", required=True),
        description=dict(type="str", required=False),
    )


# -----------------------------------------------------------------------------


class TruenasCronjobModule(TruenasModule):
    def __init__(self, module_helper, api_client, params):
        super().__init__(module_helper, api_client, params)

    # -------------------------------------------------------------------------

    def run_module(self):
        self.module_helper.handle_check_mode(changed=False, response="")
        self.module_helper.set_request(self.data)

        state = self.get_param("state")
        cronjob = self.api_client.find_item_by_property(
            "cronjob", "command", self.get_param("command")
        )
        cronjob_id = ""
        cronjob_exists = False

        if cronjob is not None and cronjob["id"] is not None:
            cronjob_exists = True
            cronjob_id = int(cronjob["id"])
            api_endpoint = f"cronjob/id/{cronjob_id}"
        else:
            api_endpoint = "cronjob"

        if state == "present" and cronjob_exists:
            self.api_client.put(api_endpoint, self.data)
            response = self.get_cronjob(cronjob_id)
            self.module_helper.set_response(response)
            has_changed = self.module_helper.has_changed(cronjob, response)
            self.module_helper.set_changed(has_changed)

        elif state == "absent" and cronjob_exists:
            self.api_client.delete(api_endpoint)
            self.module_helper.set_response(
                f"The cronjob with the cronjob id {cronjob_id} was deleted"
            )
            self.module_helper.set_changed(True)

        elif state == "present" and not cronjob_exists:
            cronjob_id = self.api_client.post("cronjob", self.data)
            self.module_helper.check_for_error(cronjob_id)

            new_cronjob = self.get_cronjob(cronjob_id)
            self.module_helper.set_response(new_cronjob)
            self.module_helper.set_changed(True)

        elif state == "absent" and not cronjob_exists:
            self.module_helper.set_response("Cronjob was already absent")
            self.module_helper.set_changed(False)

        self.module_helper.exit_json()

    # -------------------------------------------------------------------------

    def get_cronjob(self, cronjob_id):
        return self.api_client.find_item_by_property("cronjob", "id", cronjob_id)

    def set_data(self):
        self.data = {
            "schedule": {
                "minute": self.get_param("minute"),
                "hour": self.get_param("hour"),
                "dom": self.get_param("dom"),
                "month": self.get_param("month"),
                "dow": self.get_param("dow"),
            }
        }
        self.add_param_to_data("enabled")
        self.add_param_to_data("stdout")
        self.add_param_to_data("command")
        self.add_param_to_data("user")
        self.add_param_to_data("description")


# -----------------------------------------------------------------------------


def main():
    module_args = get_module_args()
    module = AnsibleModuleHelper.get_module(module_args)
    module_helper = AnsibleModuleHelper(module)
    module_helper.set_no_log_values("api_token")

    base_url = module_helper.get_param("url")
    verify = module_helper.get_param("verify")
    api_token = module_helper.get_param("api_token")
    api_client = TruenasApiClient(base_url, api_token, bool(verify))

    params = module_helper.params
    truenas_module = TruenasCronjobModule(module_helper, api_client, params)
    truenas_module.run_module()


if __name__ == "__main__":
    main()
