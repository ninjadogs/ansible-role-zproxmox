#!/usr/bin/python

# Copyright: (c) 2018, Terry Jones <terry.jones@example.org>
# GNU General Public License v3.0+ (see COPYING or https://www.gnu.org/licenses/gpl-3.0.txt)
try:
    from ansible_collections.zkygr.zproxmox.plugins.module_utils.ansible_module_helper import (
        AnsibleModuleHelper,
    )
    from ansible_collections.zkygr.zproxmox.plugins.module_utils.truenas_api_client import (
        TruenasApiClient,
    )
    from ansible_collections.zkygr.zproxmox.plugins.module_utils.truenas_module import (
        TruenasModule,
    )
except:
    from module_utils.truenas_api_client import TruenasApiClient
    from module_utils.truenas_module import TruenasModule
    from module_utils.ansible_module_helper import AnsibleModuleHelper

import urllib.parse

__metaclass__ = type

DOCUMENTATION = r"""
---
module: truenas_pool_dataset
short_description: Create or delete datasets in truenas
version_added: "1.0.0"
description: This module can create or delete truenas datasets.

options:
    url: 
        description: The url of the truenas server.
        required: true
        type: str
    api_token: 
        description: The api token for the truenas server.
        required: true
        type: str
    verify: 
        description: Whether to verify the ssl certificate or not 
        required: false
        type: bool
    name:
        description: The name of the dataset that should be created
        required: true
        type: str
    state: 
        description: Whether to create (present) or remove (absent) the dataset.
        required: true
        type: choices ['present', 'absent']

author:
    - Kay Gerlitzki (@zkygr)
"""

EXAMPLES = r"""
# Pass in a message
- name: Create a datasets for photos
  truenas_pool_dataset:
      url: https://truenas.my.domain
      api_token: ****
      name: pool/photos
      state: present

- name: Delete a dataset
  truenas_nfs_share:
      url: https://truenas.my.domain
      api_token: ****
      name: pool/photos
      state: absent

"""


def get_module_args():
    return dict(
        url=dict(type="str", required=True),
        api_token=dict(type="str", required=True),
        verify=dict(type="bool", default=False, required=False),
        name=dict(type="str", required=True),
        state=dict(default="present", choices=["present", "absent"]),
    )


# -----------------------------------------------------------------------------


class TruenasPoolsDatasetModule(TruenasModule):
    def __init__(self, module_helper, api_client, params):
        super().__init__(module_helper, api_client, params)

    def run_module(self):
        self.module_helper.set_request(self.data)
        self.module_helper.handle_check_mode(changed=False, response="")
        state = self.module_helper.get_param("state")

        dataset_name = self.module_helper.get_param("name")
        dataset = self.find_dataset_by_name(dataset_name)
        if state == "present" and dataset is None:
            api_endpoint = f"pool/dataset"
            response = self.api_client.post(api_endpoint, self.data)
            self.module_helper.set_changed(True)
            self.module_helper.set_response(response)

        elif state == "present" and dataset is not None:
            self.module_helper.set_response("Dataset already exists")

        elif state == "absent" and dataset is None:
            self.module_helper.set_response("Dataset is already absent")

        elif state == "absent" and dataset is not None:
            dataset_id = urllib.parse.quote(dataset["id"], safe="")
            api_endpoint = f"pool/dataset/id/{dataset_id}"
            self.api_client.delete(api_endpoint)
            self.module_helper.set_changed(True)

        self.module_helper.exit_json()

    def find_dataset_by_name(self, name):
        api_endpoint = "pool/dataset"
        dataset = self.api_client.find_item_by_property(api_endpoint, "name", name)

        return dataset

    def set_data(self):
        self.data = {
            "name": self.module_helper.get_param("name"),
        }


# -----------------------------------------------------------------------------


def main():
    module_args = get_module_args()
    module = AnsibleModuleHelper.get_module(module_args)
    module_helper = AnsibleModuleHelper(module)

    base_url = module_helper.get_param("url")
    verify = module_helper.get_param("verify")
    api_token = module_helper.get_param("api_token")
    api_client = TruenasApiClient(base_url, api_token, bool(verify))

    params = module_helper.params
    truenas_module = TruenasPoolsDatasetModule(module_helper, api_client, params)
    truenas_module.run_module()


if __name__ == "__main__":
    main()
