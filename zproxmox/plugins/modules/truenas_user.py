#!/usr/bin/python

# Copyright: (c) 2018, Terry Jones <terry.jones@example.org>
# GNU General Public License v3.0+ (see COPYING or https://www.gnu.org/licenses/gpl-3.0.txt)
try:
    from ansible_collections.zkygr.zproxmox.plugins.module_utils.ansible_module_helper import (
        AnsibleModuleHelper,
    )
    from ansible_collections.zkygr.zproxmox.plugins.module_utils.truenas_api_client import (
        TruenasApiClient,
    )
    from ansible_collections.zkygr.zproxmox.plugins.module_utils.truenas_module import (
        TruenasModule,
    )
except:
    from module_utils.truenas_api_client import TruenasApiClient
    from module_utils.truenas_module import TruenasModule
    from module_utils.ansible_module_helper import AnsibleModuleHelper

__metaclass__ = type

DOCUMENTATION = r"""
---
module: truenas_user
short_description: Create or delete user in truenas.
version_added: "1.0.0"
description: This module can create or delete truenas users.

options:
    url: 
        description: The url of the truenas server.
        required: true
        type: str
    api_token: 
        description: The api token for the truenas server.
        required: true
        type: str
    verify: 
        description: Whether to verify the ssl certificate or not 
        required: false
        type: bool
    state: 
        description: Whether to create (present) or remove (absent) the user.
        required: true
        type: choices ['present', 'absent']
    username: 
        description: Username of the User.
        required: false
        type: str
    uid: 
        description: If `uid` is not provided it is automatically filled with the next one available.
        required: false
        type: str
    shell: 
        description: Available choices for `shell`.
        required: true
        type: choices=[ "/usr/sbin/nologin",
                "/usr/bin/bash",
                "/usr/bin/zsh",
                "/usr/bin/tmux",
                "/usr/bin/rbash",
                "/usr/bin/dash",
            ]
    password_disabled: 
        description: `password` is required if `password_disabled` is false.
        required: false
        type: bool
    password: 
        description: `password` is required if `password_disabled` is false.
        required: false
        type: str
    group_create: 
        description: `group` is required if `group_create` is false.        
        required: false
        type: bool
    group: 
        description: `group` is required if `group_create` is false.
        required: false
        type: bool
    attributes: 
        description: attributes` is a general-purpose object for storing arbitrary user information.
        required: false
        type: dict
    full_name: 
        description: Full name of the User.
        required: true
        type: str
    home_create: 
        description: If true a home directory will be created.
        required: false
        type: bool
    home: 
        description: The path to the home directory.
        required: false
        type: str
    home_mode: 
        description: The mode of the home directory.
        required: false
        type: str
    email: 
        description: Email address of the user.
        required: false
        type: str
    sudo_commands: 
        description: List of allowed sudo commands
        required: false
        type: list
    sudo_commands_nopasswd: 
        description: List of allowed sudo commands without password
        required: false
        type: list
    sshpubkey:
        description: The ssh public key of the user.
        required: false
        type: str

author:
    - Kay Gerlitzki (@zkygr)
"""

EXAMPLES = r"""
# Pass in a message
"""


def get_module_args():
    return dict(
        url=dict(type="str", required=True),
        api_token=dict(type="str", required=True),
        verify=dict(type="bool", default=False, required=False),
        state=dict(default="present", choices=["present", "absent"]),
        uid=dict(type="str", required=False),
        username=dict(type="str", required=True),
        full_name=dict(type="str", required=False),
        shell=dict(
            default="/usr/sbin/nologin",
            choices=[
                "/usr/sbin/nologin",
                "/usr/bin/bash",
                "/usr/bin/zsh",
                "/usr/bin/tmux",
                "/usr/bin/rbash",
                "/usr/bin/dash",
            ],
        ),
        password_disabled=dict(type="bool", required=False),
        password=dict(type="str", required=False),
        group_create=dict(type="bool", required=False),
        group=dict(type="str", required=False),
        groups=dict(type="list", elements=str, required=False),
        attributes=dict(type="dict", required=False),
        home_create=dict(type="bool", required=False),
        home=dict(type="str", required=False),
        home_mode=dict(type="str", required=False),
        email=dict(type="str", required=False),
        sudo_commands=dict(type="list", elements=str, required=False),
        sudo_commands_nopasswd=dict(type="list", elements=str, required=False),
        sshpubkey=dict(type="str", required=False),
    )


# -----------------------------------------------------------------------------


class TruenasUserModule(TruenasModule):
    def __init__(self, module_helper, api_client, params):
        super().__init__(module_helper, api_client, params)

    # -------------------------------------------------------------------------

    def run_module(self):
        self.module_helper.handle_check_mode(changed=False, response="")
        self.module_helper.set_request(self.data)

        state = self.get_param("state")
        user = self.api_client.find_item_by_property(
            "user", "username", self.get_param("username")
        )
        user_id = ""
        user_exists = False

        if user is not None and user["id"] is not None:
            user_exists = True
            user_id = int(user["id"])
            api_endpoint = f"user/id/{user_id}"
        else:
            api_endpoint = "user"

        if state == "present" and user_exists:
            self.remove_from_data("group_create")
            self.api_client.put(api_endpoint, self.data)
            response = self.get_user(user_id)
            self.module_helper.set_response(response)
            has_changed = self.module_helper.has_changed(user, response)
            self.module_helper.set_changed(has_changed)

        elif state == "absent" and user_exists:
            self.api_client.delete(api_endpoint)
            self.module_helper.set_response(
                f"The user with the user id {user_id} was deleted"
            )
            self.module_helper.set_changed(True)

        elif state == "present" and not user_exists:
            user_id = self.api_client.post("user", self.data)
            self.module_helper.check_for_error(user_id)

            new_user = self.get_user(user_id)
            self.module_helper.set_response(new_user)
            self.module_helper.set_changed(True)

        elif state == "absent" and not user_exists:
            self.module_helper.set_response("User was already absent")
            self.module_helper.set_changed(False)

        self.module_helper.exit_json()

    # -------------------------------------------------------------------------

    def get_user(self, user_id):
        return self.api_client.find_item_by_property("user", "id", user_id)

    def set_data(self):
        attributes = self.get_param("attributes", False)
        if attributes is None:
            attributes = {}

        groups = self.get_param("groups", False)
        if groups is not None:
            self.add_param_to_data("groups", groups)

        self.add_param_to_data("shell")
        self.add_param_to_data("full_name")
        self.add_param_to_data("username")
        self.add_param_to_data("attributes", attributes)
        self.add_param_to_data("uid")
        self.add_param_to_data("email")
        self.add_param_to_data("sudo_commands")
        self.add_param_to_data("sudo_commands_nopasswd")
        self.add_param_to_data("sshpubkey")

        self.add_conditional_data("group_create", False, "group")
        self.add_conditional_data("password_disabled", False, "password")
        self.add_conditional_data("home_create", True, "home")
        self.add_conditional_data("home_create", True, "home_mode")


# -----------------------------------------------------------------------------


def main():
    module_args = get_module_args()
    module = AnsibleModuleHelper.get_module(module_args)
    module_helper = AnsibleModuleHelper(module)

    base_url = module_helper.get_param("url")
    verify = module_helper.get_param("verify")
    api_token = module_helper.get_param("api_token")
    api_client = TruenasApiClient(base_url, api_token, bool(verify))

    params = module_helper.params
    truenas_module = TruenasUserModule(module_helper, api_client, params)
    truenas_module.run_module()


if __name__ == "__main__":
    main()
