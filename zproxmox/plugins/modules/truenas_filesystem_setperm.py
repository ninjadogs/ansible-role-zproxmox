#!/usr/bin/python

# Copyright: (c) 2018, Terry Jones <terry.jones@example.org>
# GNU General Public License v3.0+ (see COPYING or https://www.gnu.org/licenses/gpl-3.0.txt)
try:
    from ansible_collections.zkygr.zproxmox.plugins.module_utils.ansible_module_helper import (
        AnsibleModuleHelper,
    )
    from ansible_collections.zkygr.zproxmox.plugins.module_utils.truenas_api_client import (
        TruenasApiClient,
    )
    from ansible_collections.zkygr.zproxmox.plugins.module_utils.truenas_module import (
        TruenasModule,
    )
except:
    from module_utils.truenas_api_client import TruenasApiClient
    from module_utils.truenas_module import TruenasModule
    from module_utils.ansible_module_helper import AnsibleModuleHelper


__metaclass__ = type

DOCUMENTATION = r"""
---
module: truenas_filesystem_setacl
short_description: Set the permission of a dataset.
version_added: "1.0.0"
description: This module sets the permissions of a dataset.

options:
    url: 
        description: The url of the truenas server.
        required: true
        type: str
    api_token: 
        description: The api token for the truenas server.
        required: true
        type: str
    verify: 
        description: Whether to verify the ssl certificate or not 
        required: false
        type: bool
    recursive:
        description: Apply the ACL recursively
        required: false
        default: false
        type: bool
    traverse:
        description: Traverse filestem boundaries (ZFS datasets)
        required: false
        default: false
        type: bool
    uid:
        description: The user id of the user who should own the dataset
        required: true
        type: str
    gid: 
        description:  The group id of the group which should own the dataset
        required: true
        type: str
    mode:
        description: Will be applied to the path and files and subdirectories depending on which options are selected 
        required: true
        type: str
    stripacl:
        description: Setperm will fail if an extended ACL is present on path, unless stripacl is set to True.        
        required: false
        type: bool

author:
    - Kay Gerlitzki (@zkygr)
"""

EXAMPLES = r"""
# Pass in a message
- name: Create a datasets for photos
  truenas_pool_dataset:
      url: https://truenas.my.domain
      api_token: ****
      name: pool1/photos
      verify: true
      state: present

- name: Delete a dataset
  truenas_nfs_share:
      url: https://truenas.my.domain
      api_token: ****
      name: pool1/photos
      verify: true
      state: absent

"""


def get_module_args():
    return dict(
        url=dict(type="str", required=True),
        api_token=dict(type="str", required=True),
        verify=dict(type="bool", default=False, required=False),
        path=dict(type="str", required=True),
        uid=dict(type="str", required=True),
        gid=dict(type="str", required=True),
        mode=dict(type="str", required=True),
        recursive=dict(type="bool", default=False, required=False),
        traverse=dict(type="bool", default=False, required=False),
        stripacl=dict(type="bool", default=False, required=False),
    )


# -----------------------------------------------------------------------------


class TruenasSetpermModule(TruenasModule):
    def __init__(self, module_helper, api_client, params):
        super().__init__(module_helper, api_client, params)

    def run_module(self):
        self.module_helper.set_request(self.data)
        self.module_helper.handle_check_mode(changed=False, response="")
        path = self.module_helper.get_param("path")

        old_acl_config = self.api_client.post("filesystem/getacl", {"path": path})
        if old_acl_config is None or old_acl_config.get("path") is None:
            self.module_helper.set_changed(False)
            self.module_helper.set_response("There was no dataset.", False)
            self.module_helper.exit_json()

        job_id = self.api_client.post("filesystem/setperm", self.data)
        timeout_in_sec = 20
        jobs_data, is_success, error_msg = self.api_client.wait_until_job_is_finished(
            job_id, timeout_in_sec
        )
        if not is_success:
            self.module_helper.fail_json(error_msg)
        self.module_helper.check_for_error(jobs_data)

        new_acl_config = self.api_client.post("filesystem/getacl", {"path": path})

        # process results
        self.module_helper.set_request(old_acl_config)
        self.module_helper.set_response(new_acl_config)
        has_changed = sorted(old_acl_config.items()) != sorted(new_acl_config.items())
        self.module_helper.set_changed(has_changed)
        self.module_helper.exit_json()

    # -------------------------------------------------------------------------

    def set_data(self):
        self.data = {
            "path": self.module_helper.get_param("path"),
            "uid": self.module_helper.get_param("uid"),
            "gid": self.module_helper.get_param("gid"),
            "options": {
                "recursive": self.module_helper.get_param("recursive"),
                "stripacl": self.module_helper.get_param("stripacl"),
                "traverse": self.module_helper.get_param("traverse"),
            },
            "mode": self.module_helper.get_param("mode"),
        }


# -----------------------------------------------------------------------------


def main():
    module_args = get_module_args()
    module = AnsibleModuleHelper.get_module(module_args)
    module_helper = AnsibleModuleHelper(module)

    base_url = module_helper.get_param("url")
    verify = module_helper.get_param("verify")
    api_token = module_helper.get_param("api_token")
    api_client = TruenasApiClient(base_url, api_token, bool(verify))

    params = module_helper.params
    truenas_module = TruenasSetpermModule(module_helper, api_client, params)
    truenas_module.run_module()


if __name__ == "__main__":
    main()
