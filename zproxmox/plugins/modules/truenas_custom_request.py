# Copyright: (c) 2018, Terry Jones <terry.jones@example.org>
# GNU General Public License v3.0+ (see COPYING or https://www.gnu.org/licenses/gpl-3.0.txt)
try:
    from ansible_collections.zkygr.zproxmox.plugins.module_utils.ansible_module_helper import (
        AnsibleModuleHelper,
    )
    from ansible_collections.zkygr.zproxmox.plugins.module_utils.truenas_api_client import (
        TruenasApiClient,
    )
    from ansible_collections.zkygr.zproxmox.plugins.module_utils.truenas_module import (
        TruenasModule,
    )
except:
    from module_utils.truenas_api_client import TruenasApiClient
    from module_utils.truenas_module import TruenasModule
    from module_utils.ansible_module_helper import AnsibleModuleHelper

__metaclass__ = type

DOCUMENTATION = r"""
---
module: truenas_custom_request
short_description: Send a custom request to truenas.
version_added: "1.0.0"
description: Use this module to send requests to truenas which are not supported by other modules. This module always returns changed = true

options:
    url: 
        description: The url of the truenas server.
        required: true
        type: str
    api_token: 
        description: The api token for the truenas server.
        required: true
        type: str
    verify: 
        description: Whether to verify the ssl certificate or not 
        required: false
        type: bool
    method:
        description: Choose the HTTP Method for the request.
        required: true
        type: choices ['GET', 'POST', 'PUT', 'DELETE']
    endpoint:
        description: The endpoint to which the request is send.
        required: true
        type: str
    data:
        description: The data that is send to the endpoint.
        required: false
        type: dict



author:
    - Kay Gerlitzki (@zkygr)
"""

EXAMPLES = r"""
# Pass in a message
- name: Create a datasets for photos
  truenas_custom_request:
      url: https://truenas.my.domain
      api_token: ****
      verify: true
      method: GET
      endpoint: core/ping

- name: Send custom request
  truenas_custom_request:
    url: https://truenas.my.domain
    api_token: ****
    verify: true
    method: POST
    endpoint: filesystem/getacl
    data:
      path: /mnt/path/to/dataset
"""


def get_module_args():
    return dict(
        url=dict(type="str", required=True),
        api_token=dict(type="str", required=True),
        verify=dict(type="bool", default=False, required=False),
        method=dict(choices=["GET", "POST", "PUT", "DELETE"], required=True),
        endpoint=dict(type="str", required=True),
        data=dict(type="dict", required=False),
    )


# -----------------------------------------------------------------------------


class TruenasCustomRequestModule(TruenasModule):
    def __init__(self, module_helper, api_client, params):
        super().__init__(module_helper, api_client, params)

    def run_module(self):
        self.module_helper.handle_check_mode(changed=False, response="")

        method = self.module_helper.get_param("method")
        endpoint = self.module_helper.get_param("endpoint")
        self.module_helper.set_request(self.data)
        self.module_helper.set_changed(True)

        if method == "GET":
            if self.data == None:
                self.data = {}

            response = self.api_client.get(endpoint, self.data)

        elif method == "POST":
            response = self.api_client.post(endpoint, self.data)

        elif method == "PUT":
            response = self.api_client.put(endpoint, self.data)

        elif method == "DELETE":
            response = self.api_client.delete(endpoint)

        else:
            response = self.module_helper.fail_json("Unkown Method")

        self.module_helper.set_response(response)
        self.module_helper.exit_json()

    def set_data(self):
        self.data = self.module_helper.get_param("data")


# -----------------------------------------------------------------------------


def main():
    module_args = get_module_args()
    module = AnsibleModuleHelper.get_module(module_args)
    module_helper = AnsibleModuleHelper(module)

    base_url = module_helper.get_param("url")
    verify = module_helper.get_param("verify")
    api_token = module_helper.get_param("api_token")
    api_client = TruenasApiClient(base_url, api_token, bool(verify))

    params = module_helper.params
    truenas_module = TruenasCustomRequestModule(module_helper, api_client, params)
    truenas_module.run_module()


if __name__ == "__main__":
    main()
