# 🚀 zproxmox.zprovision

**NOTE: This Role works currently only with debian systems**

This role installs docker, connects to a nfs share and installs common packages.

## 🪄 Examples

### Define a virtual machine with ansible variables

See a list of all variables in [defaults/main.yml](./defaults.main.yml)

```yaml
# group_vars/all/zprovision.yml

zprovision__mount_src: truenas.my.home:/mnt/application/influxdb
zprovision__mount_path: /mnt/truenas/influxdb

# doup:linux-amd64
zprovision__portainer_image: portainer/agent:linux-amd64-2.16.2
zprovision__portainer_port_public: 9001
zprovision__portainer_restart_policy: always

zprovision__packages_to_install:
  - tree
  - htop
```

```yaml
# playbook.yml
- name: Provision my virtual machine
  hosts: influxdbservers
  gather_facts: false

  roles:
    - role: zkygr.zproxmox.zprovision
```

### Provision the machine

```bash
# bash
ansible-playbook playbook.yml
```
