# 🚀 zproxmox.zvm

This role combines the ansible module [proxmox_kvm](https://docs.ansible.com/ansible/latest/collections/community/general/proxmox_kvm_module.html) with the proxmox cli tool [qm](https://pve.proxmox.com/pve-docs/qm.1.html).
The result is a **idempotent** role that can create, update and delete virtual machines on a proxmox server.

## 🪄 Examples

### Define a virtual machine with ansible variables

See a list of all variables in [defaults/main.yml](./defaults.main.yml).

```yaml
# group_vars/all/zvm.yml

# node
zvm__proxmox_node: pve
zvm__proxmox_api_host: 192.168.1.47
zvm__proxmox_api_user: "{{ vault_api_user }}"
zvm__proxmox_api_pass: "{{ vault_api_pass }}"

# vm
zvm__name: best-vm-ever
zvm__template: debian-11-generic-amd64-20220711-1073

# cloudinit
zvm__ci_user: "{{ vault_ci_user }}"
zvm__ci_pass: "{{ vault_ci_pass }}"
zvm__ci_ipconfig0: "ip=192.168.1.100,gw=192.168.1.1"
zvm__ci_sshkeys: "{{ vault_ssh_pub_key }}"

# network address (ip or fqdn) of the vm to check when the vm is reachable
zvm__ssh_host: 192.168.1.100
```

```yaml
# playbook.yml
- name: Create a virtual machine
  hosts: proxmox
  gather_facts: false

  roles:
    - role: zkygr.zproxmox.zvm
```

### Create a virtual machine

```bash
# bash
ansible-playbook --tags zvm_create playbook.yml
```

### Delete a virtual machine

```bash
# bash
ansible-playbook --tags zvm_delete playbook.yml
```

### Update a virtual machine

```yaml
# group_vars/all/zvm.yml
zvm__hw_cores: 4
zvm__hw_memory: 4096
```

```bash
# bash
ansible-playbook --tags zvm_update playbook.yml
```
