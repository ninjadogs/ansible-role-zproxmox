# 🚀 zkygr.zproxmox.zcompose

`zcomposes` is here to help you managing your docker stack.
The following tasks are implemented:

- add nfs mounts
- ensure that all volume-mount-points exists
- starting a docker stack
- removing a docker stack
- list all running containers
- create a borg repo and a cronjob for scheduled backups
- create a cronjob to re

## 🤖 Configuration

See a list of all variables in [defaults/main.yml](./defaults/main.yml).

```bash
# group_vars/server1/zcompose.yml

# docker compose
zcompose__docker_compose_src: templates/docker-compose.yml.j2
zcompose__docker_compose_dest: /etc/docker

# Ensure volume directories exists and have the right owner/group
zcompose__default_owner: "{{ applicationxyz_user_id }}"
zcompose__default_group: "{{ applicationxyz_group_id }}"
zcompose__volumes:
    - dest: "{{ applicationxyz_dir }}"
    - dest: "{{ applicationxyz_config_dir }}"

# copy config files
zcompose__config_files:
    - src: templates/template_a.cfg.j2
      dest: "{{ applicationxyz_config_dir }}"
      owner: "{{ applicationxyz_user_id }}"
      group: "{{ applicationxyz_group_id }}"
      mode: "{{ applicationxyz_config_file_mode }}"
```

## 🪄 Examples

```bash
# stating a docker stack
ansible-playbook -i inventory.yml --tags=zcompose_start playbook.yml

# removing a docker stack
ansible-playbook -i inventory.yml --tags=zcompose_remove playbook.yml

# list all containers
ansible-playbook -i inventory.yml --tags=zcompose_list playbook.yml
```

### 🤖 Borgbackup

Create a backup in `/tmp/borg` for the following path: `/mnt/nfs/stack1/application`:

```yaml
zcompose__borg_skip_backup: false
zcompose__borg_repo_path: "/tmp/borg"
zcompose__borg_repo_name: "stack1-application-repo"
zcompose__borg_backup_path: "/mnt/nfs/stack1/application"
```

Backup the same directory but stop the docker-stack before.

```yaml
zcompose__borg_docker_compose_dir: "/path/to/docker-compose"

zcompose__borg_skip_backup: false
zcompose__borg_repo_path: "/tmp/borg"
zcompose__borg_repo_name: "stack1-application-repo"
zcompose__borg_backup_path: "/mnt/nfs/stack1/application"
```
